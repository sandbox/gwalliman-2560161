<?php

function appxtender_api_admin_form($form, &$form_state)
{
  $appx_config = variable_get('appxtender_api_config', array());

  $form = array(
    'appx_url' => array(
      '#type' => 'textfield',
      '#title' => 'Appxtender Base URL',
      '#description' => 'The base URL for your Appx Instance. YOU MUST INCLUDE THE TRAILING SLASH',
      '#default_value' => isset($appx_config['appx_url']) ? $appx_config['appx_url'] : '',
    ),
    'appx_username' => array(
      '#type' => 'textfield',
      '#title' => 'Appxtender Username',
      '#default_value' => isset($appx_config['appx_username']) ? $appx_config['appx_username'] : '',
    ),
    'appx_password' => array(
      '#type' => 'textfield',
      '#title' => 'Appxtender Password',
      '#default_value' => isset($appx_config['appx_password']) ? $appx_config['appx_password'] : '',
    ),
    'submit' => array(
      '#type' => 'submit',
      '#value' => 'Submit',
    ),
  );

  return $form;
}

function appxtender_api_admin_form_submit($form, &$form_state)
{
  $appx_config = variable_get('appxtender_api_config', array());
  $appx_config['appx_url'] = $form_state['values']['appx_url'];
  $appx_config['appx_username'] = $form_state['values']['appx_username'];
  $appx_config['appx_password'] = $form_state['values']['appx_password'];
  appxtender_api_set_configs($form_state['values']['appx_url'], $form_state['values']['appx_username'], $form_state['values']['appx_password']);
  variable_set('appxtender_api_config', $appx_config);
}

function appxtender_api_set_configs($url, $username, $password)
{
  $appx_config = variable_get('appxtender_api_config', array());
  $appx_config['appx_url'] = $url;
  $appx_config['appx_username'] = $username;
  $appx_config['appx_password'] = $password;
  variable_set('appxtender_api_config', $appx_config);
}
